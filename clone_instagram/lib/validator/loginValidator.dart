mixin LoginValidator{
  String? validateEmail(String? email){
    if(email!.isEmpty){
      return 'Email address is required';
    }
    final regex = RegExp('[^@]+@[^\.]+\..+');
    if(!regex.hasMatch(email)){
      return 'Enter a valid email!!';
    }
    return null;
  }
  String? validatePassword(String? password){
    if(password!.length <4){
      return 'Password must be more than 4 characters';
    }
    if(password.isEmpty){
      return 'please enter your password';
    }
    return null;
  }
}