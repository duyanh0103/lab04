
import 'package:clone_instagram/constant/post_json.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:clone_instagram/constant/post_json.dart';
import '../constant/story_json.dart';
import '../theme/colors.dart';
import 'package:clone_instagram/widgets/story_item.dart';

class HomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomePageState();
  }
  
}
class _HomePageState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return getBody();
  }
}

Widget getBody() {
  return SingleChildScrollView(
    child: Column(
      children: <Widget>[
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: <Widget>[
              //=============USER======================
              //tạo column
              //Wrap with padding
              Padding(
                padding: const EdgeInsets.only(
                    right: 20.0,
                    left: 15.0,
                    bottom: 10
                ),
                child: Column(
                  children: <Widget>[
                    //====avatar====
                    Container(
                      width: 70,
                      height: 70,
                      //xếp chồng các widget với nhau
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: 70,
                            height: 70,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(profile), // data trong file constant
                                    fit: BoxFit.cover
                                )),
                          ),
                          Positioned(
                            bottom: 0,
                              right: 0,
                              child: Container(
                                width: 20,
                                height: 20,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: white
                                ),
                                child: Icon(Icons.add_circle,
                                color: buttonFollowColor,
                                //size trùng với height and width
                                size: 20,),
                          ))
                        ],
                      ),
                    ),
                    // ====name user====
                    SizedBox(height: 8),
                    SizedBox(width: 70,
                      child: Text(
                        //data in file constant
                        name,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(color: white),
                      ),
                    ),
                  ],
                ),
              ),

              //=============STORY=====================
              Row(children: List.generate(stories.length, (index){

                    //Extract flutter widget
                    return StoryItem(
                      //* data cứng chứa trong file constant
                      img: stories[index]['img'],
                      name: stories[index]['name'],
                    );

                  }),
                  )

          ],),
        ),

        //========tạo khoảng cách giữa các story và newsfeed
        Divider(
          color: white.withOpacity(0.3),
        ),

        Column(children: List.generate(posts.length, (index){
          return PostItem(
            postImg: posts[index]['postImg'],
            profileImg: posts[index]['profileImg'],
            name: posts[index]['name'],
            caption: posts[index]['caption'],
            isLoved: posts[index]['isLoved'],
            likedBy: posts[index]['likedBy'],
            viewCount: posts[index]['viewCount'],
            dayAgo: posts[index]['dayAgo'],

          );
        })

        ,)

      ],
    ),
  );
}

class PostItem extends StatelessWidget {
  final String profileImg;
  final String name;
  final String postImg;
  final String? caption;
  final isLoved;
  final String? likedBy;
  final String? viewCount;
  final String? dayAgo;
  const PostItem({
    Key? key,  required this.profileImg,  required this.name,  required this.postImg, this.isLoved,  this.likedBy,  this.viewCount,  this.dayAgo,  this.caption,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      //(**)
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 15
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
                Row(children: <Widget>[
                  Container(
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: NetworkImage(profileImg),
                            fit: BoxFit.cover)
                    ),
                  ),
                  SizedBox(width: 15,),
                  Text(
                    name,
                    style: TextStyle(
                        color: white,
                        fontSize: 15,
                        fontWeight: FontWeight.w500
                    ),)
                ],),

              //dùng more_horiz for three dot icon
              Icon(Icons.more_horiz,color: white)
            ],
          ),
        ),
      //  =============Post=============
        Container(
          height: 400,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(postImg),
                fit: BoxFit.cover
            )
          ),
        ),
        SizedBox(height: 10,),
        Padding(
          padding: const EdgeInsets.only(
            left: 15,
            right: 15,
            top: 3
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  isLoved ?SvgPicture.asset("assets/images/loved_icon.svg",width: 27,)
                  :SvgPicture.asset("assets/images/love_icon.svg",width: 27,),
                  SizedBox(width: 20),
                  SvgPicture.asset("assets/images/comment_icon.svg",width: 27,),
                  SizedBox(width: 20),
                  SvgPicture.asset("assets/images/message_icon.svg",width: 27,),
                ],
              ),
              SvgPicture.asset("assets/images/save_icon.svg",width: 27,),
          ],),
        ),
        SizedBox(height: 12,),
        Padding(
          padding: const EdgeInsets.only(left: 15,right: 15),
          child: RichText(text: TextSpan(
            children: [
              //lúc này chữ sẽ bị hiện ở giữa => dùng crossAxisAlignment(**)
              TextSpan(
                text: "Liked by ",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                ),),

              TextSpan(
                text: "$likedBy ",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w700,
                ),),

              TextSpan(
                text: "and  ",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                ),),
              TextSpan(
                text: "others ",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w700,
                ),),
            ]
          )),
        ),
        SizedBox(height: 12,),
        Padding(padding: EdgeInsets.only(left: 15, right: 15),
          child: RichText(text: TextSpan(
              children: [
                //lúc này chữ sẽ bị hiện ở giữa => dùng crossAxisAlignment(**)
                TextSpan(
                  text: "$name ",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w700,
                  ),),

                TextSpan(
                  text: "$caption ",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                  ),),
              ]
          )),
        ),
        SizedBox(height: 12,),
        Padding(padding: EdgeInsets.only(left:15,right: 15),
        child: Text("View $viewCount comments", style: TextStyle(
          color: white.withOpacity(0.6),
          fontSize: 15,
          fontWeight: FontWeight.w500
        ),),
        ),
        SizedBox(height: 12,),
        Padding(padding: EdgeInsets.only(left: 15, right: 15),
          child:
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
            Row(
              children: <Widget>[
                //avt
                Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(image: NetworkImage(posts[0]['profileImg']),fit:
                      BoxFit.cover)
                  ),
                ),
                SizedBox(width: 15,),
                Text("Add a comment...", style: TextStyle(
                    color: white.withOpacity(0.6),
                    fontSize: 15,
                    fontWeight: FontWeight.w500
                ),),

              ],
            ),

            Row(children: <Widget>[
              Text("😂",style: TextStyle(
                  fontSize:
                  20
              ),),
              SizedBox(width: 10,),
              Text("😍",style: TextStyle(
                  fontSize:
                  20
              ),),
              SizedBox(width: 10,),
              Icon(Icons.add_circle,color: white.withOpacity(0.5),size: 18,)
            ],),
          ],)
        ),
        SizedBox(height: 12,),
        Padding(padding: EdgeInsets.only(left: 15,right: 15),
        child: Text("$dayAgo",style: TextStyle(
              color: white.withOpacity(0.6),
              fontSize: 10,
              fontWeight: FontWeight.w500),
          )
        ),
      ],
    );
  }
}


// Story item below => story_item.dart
// class StoryItem extends StatelessWidget {
//   //khởi tạo
//   final String img;
//   final String name;
//   const StoryItem({
//     //gán ở story item
//     Key? key, required this.img, required this.name,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Padding(padding: const EdgeInsets.only(right: 20, bottom: 10),
//         child: Column(
//           children: <Widget>[
//             //=============hình + vòng story===============
//             Container(
//               //======vòng hiện story chưa xem=======
//               width: 73,
//               height: 73,
//               decoration: BoxDecoration(
//                   shape: BoxShape.circle,
//                   gradient: LinearGradient(
//                       begin: Alignment.topCenter,
//                       end: Alignment.bottomCenter,
//                       colors: storyBorderColor)
//               ),
//               //======= avatar user =========
//               child: Padding(
//                 padding:const EdgeInsets.all(3.0),
//                 child:Container(
//                   width: 70,
//                   height: 70,
//                   decoration: BoxDecoration(
//                       border: Border.all(
//                           color: black,
//                           width: 2
//                       ),
//                       shape: BoxShape.circle,
//                       image: DecorationImage(
//                           image: NetworkImage(img,),
//                           fit: BoxFit.cover)
//                   ),
//                 ),
//               ),
//             ),
//
//             //tên  user của story
//             SizedBox(
//               height: 8,),
//             SizedBox(
//               width: 70,
//               child:
//               Text(name,
//                 overflow: TextOverflow.ellipsis,
//                 style: TextStyle(
//                   color: white,
//                 ),),
//             )
//           ],
//         )
//     );
//   }
// }