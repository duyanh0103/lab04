// TODO Implement this library.


import 'package:clone_instagram/ui/app.dart';
import 'package:flutter/material.dart';
import '../validator/loginValidator.dart';
import 'package:clone_instagram/ui/feed_screen.dart';

class LoginScreen extends StatefulWidget{
  static const route = '/login';

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginScreenState();
  }

}

class LoginScreenState extends State<StatefulWidget> with LoginValidator{
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Instagram',
                style: TextStyle(
                  fontFamily: 'Billabong',
                  fontSize: 50.0,
                ),
              ),
               Form(
                 key: formKey,
                   child: Column(
                     mainAxisSize: MainAxisSize.min,
                     children: <Widget>[
                       Padding(
                           padding: EdgeInsets.symmetric(
                             horizontal: 30,
                             vertical: 10,
                           ),
                         child:emailField() ,
                       ),
                       Padding(padding: EdgeInsets.symmetric(
                         horizontal: 30,
                         vertical: 10
                       ),
                         child: passwordField(),
                       ),
                       SizedBox(height: 20),
                       Container(
                         width: 250,
                         child: loginButton(),
                       ),
                     ],
                   ))

              //signupText()
            ],
          ),
        ),
      ),
    );
  }
  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: emailController,
      decoration: InputDecoration(
        labelText: 'Email address'
      ),
      validator: validateEmail,
    );
  }
  Widget passwordField() {
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      decoration: InputDecoration(
        labelText: 'Password'
      ),
      validator: validatePassword,
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed:validate,
        child: Text('Login')
    );
  }

// Widget signupText(){}

  void validate() {
    final form = formKey.currentState;
    if (!form!.validate()) {
      return;
    } else {
      final email = emailController.text;
      final password = passwordController.text;

      Navigator.of(context).pushReplacementNamed(feedSCreen.route, arguments: email);

      setState(() {

      });
    }
  }
}

