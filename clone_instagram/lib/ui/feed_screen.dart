// TODO Implement this library.
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:async';
import './login_screen.dart';
import 'package:clone_instagram/theme/colors.dart';
import 'home_page.dart';


class feedSCreen extends StatefulWidget {
  static const route = '/feedscreen';
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return feedScreenState();
  }
}

class feedScreenState extends State<StatefulWidget> {
  int pageIndex = 0;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      //appbar
      appBar: getAppBar(),
      backgroundColor: black,
      //body
      body: getBody(),
      //footer
      bottomNavigationBar: getFooter(),
    );
  }
  Widget getBody(){

    List<Widget> pages = [
      HomePage(),
      //home page
      Center(
        child: Text("Home Page",style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: white,
        ),),
      ),


      Center(
        child: Text("Search Page",style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: white,
        ),),
      ),

      Center(
        child: Text("Upload Page",style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: white,
        ),),
      ),

      Center(
        child: Text("Activity Page",style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: white,
        ),),
      ),

      Center(
        child: Text("Account Page",style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: white,
        ),),
      )

    ];
    return IndexedStack(
      index: pageIndex,
      children: pages,
    );
  }
  //AppBar
  getAppBar(){
      if(pageIndex == 0){
        return AppBar(
          backgroundColor: appBarColor,
          title:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children:<Widget>[
            Text("Instagram",style: TextStyle(
                fontFamily: 'Billabong',
                fontSize: 40
            ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child:Row(
              children: <Widget>[
                SvgPicture.asset("assets/images/camera_icon.svg",width: 30,),

                SvgPicture.asset("assets/images/message_icon.svg",width: 30,)
              ],
            ),)

          ]
          ),

        );
      }
      //search
      else if(pageIndex == 1){
        return null;
      }
      //upload
      else if(pageIndex == 2){
        return AppBar(
          backgroundColor: appBarColor,
          title: Text("Upload"),
          //căn giữa
          centerTitle: true
        );
      }
      //love ('TYM')
      else if(pageIndex == 3){
        return AppBar(
          backgroundColor: appBarColor,
          title: Text("Activity"),
            centerTitle: true
        );
      }
      //profile
      else if(pageIndex == 4){
        return AppBar(
            backgroundColor: appBarColor,
            title: Text("Account"),
            centerTitle: true
        );
      }

  }

  //tạo getFooter => cho bottom navigation bar
  Widget getFooter() {
    List bottomItems = [
      //nếu click vào thì sáng ô đó lên
      pageIndex == 0
          ? "assets/images/home_active_icon.svg"
          : "assets/images/home_icon.svg",

      pageIndex == 1
          ? "assets/images/search_active_icon.svg"
          : "assets/images/search_icon.svg",

      pageIndex == 2
          ? "assets/images/upload_active_icon.svg"
          : "assets/images/upload_icon.svg",

      pageIndex == 3
          ? "assets/images/love_active_icon.svg"
          : "assets/images/love_icon.svg",

      pageIndex == 4
          ? "assets/images/account_active_icon.svg"
          : "assets/images/account_icon.svg",
    ];
    return Container(
      width: double.infinity,
      height: 50,
      decoration: BoxDecoration(color: appFooterColor),
      child: Padding(
        padding:
            const EdgeInsets.only(
                left: 10,
                right: 10,
                bottom: 10,
                top: 15),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                //5 phần tử => chứa tham số index
            children: List.generate(5, (index) {
              return InkWell(
                  onTap: () {
                    selectedTab(index);
                  },
                  child: SvgPicture.asset(
                    bottomItems[index],
                    width: 30,
                  ));
            })),
      ),
    );
  }

  selectedTab(index) {
    setState(() {
      pageIndex = index;
    });
  }


}
