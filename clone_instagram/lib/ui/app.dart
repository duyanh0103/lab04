import 'package:flutter/material.dart';
import './login_screen.dart';
import './feed_screen.dart';
class CloneInstaApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      routes: {
        //key: đặt tên '/' là root name
        '/': (context) => LoginScreen(),
        '/login': (context) => LoginScreen(),
        //để tạm nha xíu xóa :)
        //  (context) => feedSCreen(),
        feedSCreen.route: (context) => feedSCreen()
      },
      initialRoute: '/',
    );
  }
}
