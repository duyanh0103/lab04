import 'package:flutter/material.dart';
import 'package:clone_instagram/theme/colors.dart';

class StoryItem extends StatelessWidget {
  //khởi tạo
  final String img;
  final String name;
  const StoryItem({
    //gán ở story item
    Key? key, required this.img, required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.only(right: 20, bottom: 10),
        child: Column(
          children: <Widget>[
            //=============hình + vòng story===============
            Container(
              //======vòng hiện story chưa xem=======
              width: 73,
              height: 73,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: storyBorderColor)
              ),
              //======= avatar user =========
              child: Padding(
                padding:const EdgeInsets.all(3.0),
                child:Container(
                  width: 70,
                  height: 70,
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: black,
                          width: 2
                      ),
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(img,),
                          fit: BoxFit.cover)
                  ),
                ),
              ),
            ),

            //tên  user của story
            SizedBox(
              height: 8,),
            SizedBox(
              width: 70,
              child:
              Text(name,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: white,
                ),),
            )
          ],
        )
    );
  }
}